import { NotifyManagementArgs, NotifyManagementResponse } from './../../../model/notifyManagement';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { BaseComponent } from './../base/baseComponent';
import { AllowHelper } from '../../helper/allowHelper';
import { NbDialogService } from '@nebular/theme';
import { NotifyManagementService } from '../../services/notifyManagement.service';

@Component({
  selector: 'ngx-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent extends BaseComponent implements OnInit {
  NotifyManagementEvent = {} as NotifyManagementResponse;
  NotifyManagementTemp = {} as NotifyManagementResponse;
  NotifyManagementList: NotifyManagementResponse[] = [];
  NotifyManagementArgs = {} as NotifyManagementArgs;
  typeEmail: boolean = false;
  typeLine: boolean = false;

  constructor(
    private service: NotifyManagementService,
    private dialogService: NbDialogService,
    protected allow: AllowHelper,
  ) {
    super(allow);
   }

  ngOnInit(): void {
    this.GetListNotifyManagement();
  }

  GetListNotifyManagement() {
    this.NotifyManagementArgs.PageSize = this.pageSize;
    this.NotifyManagementArgs.PageIndex = this.pageIndex;
    this.service.GetListNotifyManagement(this.NotifyManagementArgs).subscribe((res) => {
      this.totalRecords = res.TotalItems;
      this.NotifyManagementList = res.Entries;
    });
  }

  AddEvent() {
    if (this.typeEmail == true && this.typeLine == false) {
      this.NotifyManagementEvent.cType = 1;
    } else if (this.typeEmail == false && this.typeLine == true) {
      this.NotifyManagementEvent.cType = 2;
    } else if (this.typeEmail == true && this.typeLine == true) {
      this.NotifyManagementEvent.cType = 3;
    } else if (this.typeEmail == false && this.typeLine == false) {
      this.NotifyManagementEvent.cType = null;
    }
    this.service.AddEvent(this.NotifyManagementEvent).subscribe((res) => {
      this.GetListNotifyManagement();
    });
  }
  EditEvent() {
    if (this.typeEmail == true && this.typeLine == false) {
      this.NotifyManagementTemp.cType = 1;
    } else if (this.typeEmail == false && this.typeLine == true) {
      this.NotifyManagementTemp.cType = 2;
    } else if (this.typeEmail == true && this.typeLine == true) {
      this.NotifyManagementTemp.cType = 3;
    } else if (this.typeEmail == false && this.typeLine == false) {
      this.NotifyManagementTemp.cType = null;
    }
    this.NotifyManagementTemp.cCycle = this.NotifyManagementTemp.cCycle.toString();
    debugger
    this.service.EditEvent(this.NotifyManagementTemp).subscribe((res) => {
      this.GetListNotifyManagement();
    });
  }

  TypeEmail(checked: boolean) {
    this.typeEmail = checked;
  }

  TypeLine(checked: boolean) {
    this.typeEmail = checked;
  }

  AddDataEvent(dialog: TemplateRef<any>) {
    this.dialogService.open(dialog)
  }

  EditDataEvent(dialog3: TemplateRef<any>,cId: number) {
    this.NotifyManagementTemp.cId = cId;
    this.NotifyManagementTemp.cEventName = this.NotifyManagementList.find(x => x.cId == cId).cEventName;
    this.NotifyManagementTemp.cStatus = this.NotifyManagementList.find(x => x.cId == cId).cStatus;
    this.NotifyManagementTemp.cType = this.NotifyManagementList.find(x => x.cId == cId).cType;
    if (this.NotifyManagementTemp.cType == 1) {
      this.typeEmail = true;
      this.typeLine = false;
    } else if (this.NotifyManagementTemp.cType == 2) {
      this.typeEmail = false;
      this.typeLine = true;
    } else if (this.NotifyManagementTemp.cType == 3) {
      this.typeEmail = true;
      this.typeLine = true;
    } else if (this.NotifyManagementTemp.cType == null) {
      this.typeEmail = false;
      this.typeLine = false;
    }
    this.NotifyManagementTemp.cSendDate = this.NotifyManagementList.find(x => x.cId == cId).cSendDate;
    this.NotifyManagementTemp.cCycle = this.NotifyManagementList.find(x => x.cId == cId).cCycle;
    this.NotifyManagementTemp.cTarget = this.NotifyManagementList.find(x => x.cId == cId).cTarget;
    this.NotifyManagementTemp.cMessage = this.NotifyManagementList.find(x => x.cId == cId).cMessage;
    this.dialogService.open(dialog3)
  }

  viewEvent(dialog2: TemplateRef<any>) {
    this.dialogService.open(dialog2)
  }

  refOK(ref: any) {
    this.AddEvent();
    ref.close();
  }

  editOK(ref: any) {
    this.EditEvent();
    ref.close();
  }

  resultOK(ref: any) {
    ref.close();
  }
}
