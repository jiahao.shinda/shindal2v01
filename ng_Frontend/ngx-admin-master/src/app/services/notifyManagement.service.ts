import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { NotifyManagementArgs, NotifyManagementResponse } from '../../model/notifyManagement';
import { ResponseBase } from '../../model/ResponseBase';
import { ServiceBase } from './service-base';

@Injectable({
  providedIn: 'root'
})
export class NotifyManagementService extends ServiceBase {

  protected baseUrl = `${this.apiBaseUrl}/NotifyManagement`;
  constructor(
    http: HttpClient
  ) {
    super(http);

  }

  GetListNotifyManagement(request: NotifyManagementArgs): Observable<ResponseBase<NotifyManagementResponse[]>> {
    const url = `${this.baseUrl}/GetListNotifyManagement`;
    return this.http.post<ResponseBase<NotifyManagementResponse[]>>(
      url,
      request,
      this.httpOptions);
  }

  AddEvent(request: NotifyManagementResponse): Observable<ResponseBase<NotifyManagementResponse>> {
    const url = `${this.baseUrl}/AddEvent`;
    return this.http.post<ResponseBase<NotifyManagementResponse>>(
      url,
      request,
      this.httpOptions);
  }
  EditEvent(request: NotifyManagementResponse): Observable<ResponseBase<NotifyManagementResponse>> {
    const url = `${this.baseUrl}/EditEvent`;
    return this.http.post<ResponseBase<NotifyManagementResponse>>(
      url,
      request,
      this.httpOptions);
  }
}
