import { Page } from './page';

export class NotifyManagementArgs extends Page {
  cEventName: string | null;
  cStatus: boolean | null;
  cType: number | null;
}

export class NotifyManagementResponse {
  cId: number;
  cEventName: string | null;
  cMessage: string | null;
  cStatus: boolean | null;
  cType: number | null;
  cTarget: string | null;
  cCycle: string | null;
  cSendDate: Date | null;
}

