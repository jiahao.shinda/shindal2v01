﻿using System;
using System.Collections.Generic;

namespace App.EF.EF.dbTemplate
{ 
    public partial class TblNotify
    {
        public int cId { get; set; }
        public string? cEventName { get; set; }
        public string? cMessage { get; set; }
        public bool? cStatus { get; set; }
        public int? cType { get; set; }
        public string? cTarget { get; set; }
        public string? cCycle { get; set; }
        public DateTime? cSendDate { get; set; }
    }
}
