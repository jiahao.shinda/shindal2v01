﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace App.Model
{
    public class RequestBase
    {
        //public string ColumnName { get; set; }
        //public string OrderByMode { get; set; }
        public int PageIndex { get; set; } = 1;
        public int PageSize { get; set; } = 50;
        [JsonIgnore]
        public JWTPayload Payload { get; set; }
    }
}
