﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Model
{
    public class NotifyManagement
    {
        public List<NotifyManagementResponse> NotifyManagementResponse { get; set; } = new List<NotifyManagementResponse>();
    }

    public class NotifyManagementResponse
    {
        public int cId { get; set; }
        public string? cEventName { get; set; }
        public string? cMessage { get; set; }
        public bool? cStatus { get; set; }
        public int? cType { get; set; }
        public string? cTarget { get; set; }
        public string? cCycle { get; set; }
        public DateTime? cSendDate { get; set; }
    }
}
