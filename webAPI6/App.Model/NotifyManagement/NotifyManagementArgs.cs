﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Model
{
    public class NotifyManagementArgs: RequestBase
    {
        public string? cEventName { get; set; }
        public bool? cStatus { get; set; }
        public int? cType { get; set; }
    }
}
