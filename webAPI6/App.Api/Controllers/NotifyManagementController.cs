﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.BLL;
using App.Common;
using App.EF.EF.dbTemplate;
using App.Model;
using App.Model.Common;
using Jose;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace App.Api.Controllers
{
    [Route("api/[controller]")]
    public class NotifyManagementController : ControllerBase
    {
        private INotifyManagement _service;

        public NotifyManagementController(INotifyManagement service)
        {
            this._service = service;
        }

        [Route("GetListNotifyManagement")]
        [HttpPost]
        [AuthorizationFilter(Enum.Functions.role_management_view)]
        public async Task<ResponseBase<List<NotifyManagementResponse>>> GetListNotifyManagement([FromBody] NotifyManagementArgs Args)
        {
            JWTPayload jwtPayload = (JWTPayload)HttpContext.Items["jwtPayload"];
            return await _service.GetListNotifyManagement(Args, jwtPayload);
        }

        [Route("AddEvent")]
        [HttpPost]
        [AuthorizationFilter(Enum.Functions.role_management_view)]
        public async Task<ResponseBase<string>> AddEvent([FromBody] NotifyManagementResponse Args)
        {
            JWTPayload jwtPayload = (JWTPayload)HttpContext.Items["jwtPayload"];
            return await _service.AddEvent(Args, jwtPayload);
        }

        [Route("EditEvent")]
        [HttpPost]
        [AuthorizationFilter(Enum.Functions.role_management_view)]
        public async Task<ResponseBase<string>> EditEvent([FromBody] NotifyManagementResponse Args)
        {
            JWTPayload jwtPayload = (JWTPayload)HttpContext.Items["jwtPayload"];
            return await _service.EditEvent(Args, jwtPayload);
        }
    }
}
