﻿using App.EF.EF.dbTemplate;
using App.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace App.BLL
{
    public interface INotifyManagement
    {
        Task<ResponseBase<List<NotifyManagementResponse>>> GetListNotifyManagement(NotifyManagementArgs args, JWTPayload jwtPayload);
        Task<ResponseBase<string>> AddEvent(NotifyManagementResponse args, JWTPayload jwtPayload);
        Task<ResponseBase<string>> EditEvent(NotifyManagementResponse args, JWTPayload jwtPayload);
    }
}
