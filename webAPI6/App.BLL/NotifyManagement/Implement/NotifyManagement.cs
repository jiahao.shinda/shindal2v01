﻿
using App.EF.EF.dbTemplate;
using App.Model;
using static App.Model.NotifyManagement;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common;
using App.Enum;
using System.IdentityModel.Tokens.Jwt;
using Mapster;

namespace App.BLL
{
    public class NotifyManagement : ServiceBase, INotifyManagement
    {
        public async Task<ResponseBase<List<NotifyManagementResponse>>> GetListNotifyManagement(NotifyManagementArgs args, JWTPayload jwtPayload)
        {
            var response = new ResponseBase<List<NotifyManagementResponse>>()
            {
                Entries = new List<NotifyManagementResponse>()
            };
            await using (var context = base.dbTemplate(Enum.ConnectionMode.Slave))
            {
                var NotifyManagementResponseList = new List<NotifyManagementResponse>();
                try
                {
                    NotifyManagementResponseList = (from no in context.TblNotify
                                                    where (no.cEventName == args.cEventName || args.cEventName == null || args.cEventName == "")
                                                    && (no.cStatus == args.cStatus || args.cStatus == null)
                                                    && (no.cType == args.cType || args.cType == null)
                                                    select new NotifyManagementResponse
                                                    {
                                                        cId = no.cId,
                                                        cEventName = no.cEventName,
                                                        cMessage = no.cMessage,
                                                        cStatus = no.cStatus,
                                                        cType = no.cType,
                                                        cCycle = no.cCycle,
                                                        cTarget = no.cTarget,
                                                        cSendDate = no.cSendDate
                                                    }).OrderByDescending(x => x.cId).ToList();

                    response.TotalItems = NotifyManagementResponseList.Count();
                    response.Entries = NotifyManagementResponseList.Skip((args.PageIndex - 1) * args.PageSize).Take(args.PageSize).ToList();

                }
                catch (Exception ex)
                {
                    response.StatusCode = EnumStatusCode.Fail;
                    response.Message = ex.Message;
                    _logger.Error(string.Format("GetSwitchDetails EX Utc Now:{0}\n EX:{1}", DateTime.UtcNow.ToString("yyyy/MM/dd HH:mm:ss"), ex.ToString()));
                }
            }

            return response;
        }

        public async Task<ResponseBase<string>> AddEvent(NotifyManagementResponse Args, JWTPayload jwtPayload)
        {
            var response = new ResponseBase<string>()
            {
                Entries = string.Empty
            };
            try
            {
                using (var context = base.dbTemplate(Enum.ConnectionMode.Slave))
                {
                    if (Args != null) {
                        var NotifyItems = new TblNotify();
                        NotifyItems.cEventName = Args.cEventName;
                        NotifyItems.cMessage = Args.cMessage;
                        NotifyItems.cStatus = Args.cStatus;
                        NotifyItems.cType = Args.cType;
                        NotifyItems.cTarget = Args.cTarget;
                        NotifyItems.cCycle = Args.cCycle;
                        NotifyItems.cSendDate = Args.cSendDate;
                        context.TblNotify.Add(NotifyItems);
                    }
                    await context.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                response.StatusCode = EnumStatusCode.Fail;
                response.Message = ex.Message;
                _logger.Error(string.Format("SearchIp EX Utc Now:{0}\n EX:{1}", DateTime.UtcNow.ToString("yyyy/MM/dd HH:mm:ss"), ex.ToString()));
            }
            return response;
        }

        public async Task<ResponseBase<string>> EditEvent(NotifyManagementResponse Args, JWTPayload jwtPayload)
        {
            var response = new ResponseBase<string>()
            {
                Entries = string.Empty
            };

            try
            {
                using (var context = base.dbTemplate(Enum.ConnectionMode.Slave))
                {
                    if (Args != null)
                    {
                        var NotifyItems = (from no in context.TblNotify
                                           where no.cId == Args.cId
                                           select no)
                                           .FirstOrDefault();
                        NotifyItems.cEventName = Args.cEventName;
                        NotifyItems.cMessage = Args.cMessage;
                        NotifyItems.cStatus = Args.cStatus;
                        NotifyItems.cType = Args.cType;
                        NotifyItems.cTarget = Args.cTarget;
                        NotifyItems.cCycle = Args.cCycle;
                        NotifyItems.cSendDate = Args.cSendDate;
                        context.TblNotify.Update(NotifyItems);
                    }
                    await context.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                response.StatusCode = EnumStatusCode.Fail;
                response.Message = ex.Message;
                _logger.Error(string.Format("SearchIp EX Utc Now:{0}\n EX:{1}", DateTime.UtcNow.ToString("yyyy/MM/dd HH:mm:ss"), ex.ToString()));
            }
            return response;
        }
    }
}